import React from 'react';

const statusCodes: { [code: number]: string } = {
  400: 'Bad Request',
  404: 'This page could not be found',
  405: 'Method Not Allowed',
  500: 'Internal Server Error',
};

export interface ErrorPageProps {
  statusCode: number;
  title?: string;
}

/**
 * `Error` component used for handling errors.
 */

const ErrorPage: React.FC<ErrorPageProps> = (props) => {
  const { className, children } = props;
  return (
    <div>
      <h1 className="text-center">
        {statusCodesMap[statusCode]} {statusCode}
      </h1>
      {children}
    </div>
  );
};

ErrorPage.getInitialProps = ({ res }) => {
  const statusCode = res?.statusCode ? res.statusCode : err ? err.statusCode! : 404;
  return { statusCode };
};

export default ErrorPage;

// Beta realization

// export default class Error<P = {}> extends React.Component<P & ErrorProps> {
//   static displayName = 'ErrorPage';

//   static getInitialProps = _getInitialProps;
//   static origGetInitialProps = _getInitialProps;

//   render() {
//     const { statusCode } = this.props;
//     const title = this.props.title || statusCodes[statusCode] || 'An unexpected error has occurred';

//     return (
//       <div style={styles.error}>
//         {/* TODO: remove this once RSC supports next/head */}
//         {!process.env.__NEXT_RSC && (
//           <Head>
//             <title>
//               {statusCode ? `${statusCode}: ${title}` : 'Application error: a client-side exception has occurred'}
//             </title>
//           </Head>
//         )}
//         <div>
//           <style dangerouslySetInnerHTML={{ __html: 'body { margin: 0 }' }} />
//           {statusCode ? <h1 style={styles.h1}>{statusCode}</h1> : null}
//           <div style={styles.desc}>
//             <h2 style={styles.h2}>
//               {this.props.title || statusCode ? (
//                 title
//               ) : (
//                 <>
//                   Application error: a client-side exception has occurred (see the browser console for more information)
//                 </>
//               )}
//               .
//             </h2>
//           </div>
//         </div>
//         {process.env.__NEXT_RSC && (
//           <script>{`(self.__next_s=self.__next_s||[]).push(new Error(${statusCode}))`}</script>
//         )}
//       </div>
//     );
//   }
// }
