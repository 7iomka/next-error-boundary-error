import React from 'react';
import { AppProps } from 'next/app';
import AppBootstrap from '@/app/components/AppBootstrap';
import MainErrorBoundary from '@/common/components/errors/MainErrorBoundary';

// global app styles
import '@/styles/global.scss';

const App = (props: AppProps) => {
  return (
    <MainErrorBoundary>
      <AppBootstrap {...props} />
    </MainErrorBoundary>
  );
};

export default App;
