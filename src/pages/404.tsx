import React, { memo } from 'react';

export interface Page404Props {}

const Page404: React.FC<Page404Props> = ({}) => {
  return (
    <div>
      <h1 className="text-center">Not found 404</h1>
    </div>
  );
};

export default memo(Page404);
