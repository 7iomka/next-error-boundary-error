import React from 'react';
import AppInner from './AppInner';

const AppBootstrap = ({ Component, pageProps, err }) => {
  // const injectedPageProps = getInjectedPageProps(pageProps);
  // const { userSession, isReadyToRender, statusCode } = injectedPageProps;
  // const { isReadyToRender, statusCode } = pageProps;
  const { statusCode } = pageProps;
  return (
    <AppInner>
      <Component
        {...pageProps}
        // @ts-ignore
        error={err}
      />
    </AppInner>
  );
  // return isReadyToRender || statusCode === 404 ? (
  //   <AppInner>
  //     <Component
  //       {...pageProps}
  //       // @ts-ignore
  //       error={err}
  //     />
  //   </AppInner>
  // ) : null;
};

export default React.memo(AppBootstrap);
