import React, { memo } from 'react';
import useRouterEvents from '@/common/hooks/useRouterEvents';

const AppInner: React.FC = ({ children }) => {
  useRouterEvents();

  return <>{children}</>;
};

export default memo(AppInner);
