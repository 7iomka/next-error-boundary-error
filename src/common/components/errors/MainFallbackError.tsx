import { memo } from 'react';
import cx from 'clsx';
import styles from './error.module.scss';

interface MainFallbackErrorProps {
  error: Error;
}

const MainFallbackError = (props: MainFallbackErrorProps) => {
  const { error } = props;
  return (
    <div>
      <div>
        <h1 className={styles.errorPageTitle}>Service currently unavailable</h1>
        <div className={styles.errorPageCodeText}>
          <pre>Error 500.</pre>
        </div>
      </div>
      <div>
        <p>Try to refresh the page. Please contact our support below if the issue persists.</p>
      </div>
      {process.env.NEXT_PUBLIC_APP_STAGE !== 'production' && <ErrorDebug error={error} />}
    </div>
  );
};

export default memo(MainFallbackError);
