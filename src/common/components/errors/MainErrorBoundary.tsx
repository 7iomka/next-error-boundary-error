import React from 'react';
import { useRouter } from 'next/router';
import ErrorPage from '@/pages/_error';
import MainFallbackError from './MainFallbackError';
import ErrorBoundary, { useError } from './ErrorBoundary';

const MainErrorFallbackPage: React.FC = () => {
  const { error } = useError();
  return (
    <ErrorPage err={error} statusCode={500} isReadyToRender>
      <MainFallbackError error={error} />
    </ErrorPage>
  );
};

const MainErrorBoundary = ({ children }) => {
  const router = useRouter();
  return (
    <ErrorBoundary
      fallback={<MainErrorFallbackPage />}
      onError={(error, errorInfo) => console.error('errrorrrrrrr', { error, errorInfo })}
      onReset={() => {
        // reset the state of your app so the error doesn't happen again
      }}
      /**
       * Reset the error component when the route changes.
       */
      key={router.asPath}
    >
      {children}
    </ErrorBoundary>
  );
};

export default React.memo(MainErrorBoundary);
