import { useEffect } from 'react';
import { useRouter } from 'next/router';
import NProgress from 'nprogress';

const userRouterEvents = () => {
  const nextRouter = useRouter();

  useEffect(() => {
    const handleChangeStart = (path) => {
      NProgress.start();
    };

    const handleChangeComplete = (path) => {
      NProgress.done();
    };

    const handleChangeError = (err, path) => {
      NProgress.done();
    };

    nextRouter.events.on('routeChangeStart', handleChangeStart);
    nextRouter.events.on('routeChangeComplete', handleChangeComplete);
    nextRouter.events.on('routeChangeError', handleChangeError);

    return () => {
      nextRouter.events.off('routeChangeStart', handleChangeStart);
      nextRouter.events.off('routeChangeComplete', handleChangeComplete);
      nextRouter.events.off('routeChangeError', handleChangeError);
    };
  }, []);
};

export default userRouterEvents;
