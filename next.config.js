const withPlugins = require('next-compose-plugins');
const transpileModules = require('next-transpile-modules');

const nextConfig = {
  reactStrictMode: true,
  typescript: {
    ignoreDevErrors: true,
    ignoreBuildErrors: true,
  },
};

const plugins = [transpileModules(['ui'])];
module.exports = withPlugins(plugins, nextConfig);
